//Registers
//the VM implements a load-store-architecture for the sake of simplicity.
//there are no SIMD instructions. a full instruction is two bytes. the first byte is 
//opcode, the second byte is an address if the op is a load/store/jmp op or a constant if
//the op is an arithmetic op. If it requires no argument, the second byte is left blank.
//yes, this is inefficient, but it simplifies things.
