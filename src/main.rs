#![feature(int_to_from_bytes)]
#[macro_use]
mod logging;
mod mem;

fn main() {
	let mut memory = mem::Memory::new();
	memory.set_arbitrary(0, "DEADBEEFCAFEBABE");
	log!("{:?}", memory);

	logging::finish_logging();
}
