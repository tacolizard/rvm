//TODO:
//work out a way to log errors

pub const PRINT_LOGS: bool = true;//print everything that is logged
pub const LOG_FILE: &'static str = ".log";

#[macro_export]
macro_rules! log {//write log to file, print if configured to do so
	($($arg:tt)*) => (
		let fmted = format_log!($($arg)*);
		if crate::logging::PRINT_LOGS {
			print!("{}", fmted);
		}
		write_log!("{}", fmted);
	);
}

#[macro_export]
macro_rules! noplog {//just log ;\n for spacing purposes
	() => (
		let nlog = ";\n";
		if crate::logging::PRINT_LOGS {
			print!("{}", nlog);
		}
		write_log!("{}", nlog);
	);
}

macro_rules! format_log {//format log with {};\n
	($($arg:tt)*) => (
		format!(";{}\n", format_args!($($arg)*));
	);
}

macro_rules! write_log {//write log to file
	($($arg:tt)*) => (
		use std::io::prelude::*;

		let mut file = std::fs::OpenOptions::new()
			.write(true)
			.append(true)
			.create(true)
			.open(crate::logging::LOG_FILE)
			.unwrap();
		if let Err(e) = write!(file, $($arg)*) {
			eprintln!("Failed to write to log file: {}", e);
		}
	);
}

pub fn finish_logging() {//separate logs from separate runs
	write_log!("\n---Log boundary---\n\n");
}

//helper to get hex and dec and some formatting
pub fn fmtint<T: std::fmt::UpperHex + std::fmt::Display>(int: T) -> String {
	return format!("{:#04X};{}", int, int);
}
