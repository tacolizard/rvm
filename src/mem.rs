/*
SEGMENTS:
Segments have three purposes: segregation of the loaded program and system memory,
allowing memory locations higher than the maximum value of BASETYPE to be
accessed, and automatically compensating for changes in system memory or program size
by treating addresses as relative to their segment. Segments are sections of memory 
that can be dynamically allocated. Segments have two primary parts; the offset 
(beginning) and size. The beginning of a segment is always the end of the previous 
segment + 1. The end of a segment is computed as the offset plus the size. 
Thus, if segment five's offset is 512, and its size is 1024, then its end is computed
as 512+1024: 1536. Attempts to access memory past the end of the current segment will
fail.
	
If the current segment is four, and segment four's offset is 512, then all 
addresses will essentially be computed as ADDRESS+512. If the result of the
computation ADDRESS+512 yields a location in memory past segment four's computed
end location, the attempted access will fail. If segment four's size is 1024, then
the offset of segment five will be 1025.

In the actual VM, Segment0, the reserved system segment, is handled specially.
Segment0's offset is not based on the end of another segment. Instead, it is always
zero. Thus Segment0 always occupies the lowest portion of memory.

Segment1 also receives unique treatment. Segment1 starts after the end of the
previous segment (in this case, Segment0) just like a normal segment, but it is
dynamically sized to be able to hold the currently loaded program.
*/

//the following code uses the terms "size" and "boundary" interchangably, even though
//they have different meanings. I'm lazy
extern crate hex;
use std::vec::Vec;
use std::fmt;
use crate::logging::fmtint;

//configurable values
const MEMSIZE: usize = 4096;//total size of memory.
//Non-configurable; Don't touch
type BASETYPE = u8;//the smallest addressable unit of memory
//^^used to be configurable but currently it is not
const SYSSEGBOUND: usize = 255;//boundary of system segment 
const SYSSEG: usize = 0;//seg num of system seg
const PROGSEG: usize = 1;//seg num of program seg

//struct for individual segment
struct Segment {
	offset: usize,
	size: usize,
}
//segment resizing can't be implemented on the segment itself because resizing a segment
//changes the offset of other segments, so it is implemented on the SegmentTable
impl Segment {
	fn new(offsetIn: usize, sizeIn: usize) -> Segment {
		log!("creating Segment with offset {} and size {}", fmtint(offsetIn), fmtint(sizeIn));
		Segment {
			offset: offsetIn,
			size: sizeIn,
		}
	}
	//compute end location of segment
	fn end(&self) -> usize {
		return self.offset+self.size;
	}
	fn set_offset(&mut self, off: usize) {
		self.offset = off;
	}
	fn set_size(&mut self, siz: usize) {
		self.size = siz;
	}
}
impl fmt::Debug for Segment {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "\nSegment (offset: {}, size: {}, self.end(): {})",
			fmtint(self.offset),
			fmtint(self.size),
			fmtint(self.end()),
		)
	}
}

#[derive(Debug)]
struct SegmentTable {
	segs: Vec<Segment>,//numerically indexed segments
	maxsize: usize,//end of last segment must be less than this
	cur_seg: usize,//current segment
}

//for now, there is no way to remove a segment
impl SegmentTable {
	fn new(size: usize) -> SegmentTable {
		log!("creating SegmentTable with SYSSEGBOUND {}", fmtint(SYSSEGBOUND));
		let sysseg = Segment::new(0, SYSSEGBOUND);
		noplog!();
		SegmentTable {
			segs: vec![sysseg],//init segs with sysseg
			maxsize: size,
			cur_seg: 0,
		}
	}
	//ensure that segments fit in allocated size, panics if they don't
	fn check_size(&self) -> usize {//returns remaining space
		let size = self.segs.last().unwrap().end();
		if size > self.maxsize {
			panic!("SegmentTable ends at {}, greater than the allocated space of {}",
				fmtint(size),
				fmtint(self.maxsize),
			);
		}
		return self.maxsize-size;
	}
	fn get_cur_seg_ref(&self) -> &Segment {
		return self.get_seg_ref(self.cur_seg);
	}
	//returns end of segment table
	fn get_size(&self) -> usize {
		self.check_size();
		return self.segs.last().unwrap().end();
	}
	//get ref to a seg by numerical index
	fn get_seg_ref(&self, index: usize) -> &Segment {
		let outseg = self.segs.get(index)
			.expect("getSeg: Failed to get seg by num index");
		return outseg;
	}
	//internal function for adding segs
	fn add_seg(&mut self, seg: Segment) -> usize {
		log!("pushing Segment into SegmentTable at index {}", fmtint(self.segs.len()));
		self.segs.push(seg);//push seg
		self.check_size();
		return self.segs.len();//get new length of table
	}
	//basic segment creation, segments can only be created at the end of the table
	fn create_seg(&mut self, size: usize) -> usize {
		log!("attempting to create Segment{} with size {}", 
			fmtint(self.segs.len()),
			fmtint(size),
		);
		if size == 0 { panic!("create_seg: attempted to create seg with size 0"); }
		let lastseg = self.segs.last()//get last segment
			.expect("create_seg: Failed to get last seg in table");
		//compute offset of new seg and create it
		let newseg = Segment::new(lastseg.end()+1, size);
		log!("created Segment{}, ending at {}", self.segs.len(), fmtint(newseg.end()));
		return self.add_seg(newseg);
	}
	fn resolve_layout(&mut self) {	
		//algorithm: iterate through segs, counting by one, starting at 0
		//each loop retrieves two segs. 
		//compare end of first seg(a) to offset of second(b) 
		log!("resolving layout of a SegmentTable consisting of {} Segments", self.segs.len());
		let mut i = 0usize;
		while (i < self.segs.len()) && (i+1 < self.segs.len()) {
			log!("resolving Segments {} and {}", i, i+1);
			//get zeroth segment
			let seg0 = self.get_seg_ref(i);
			i+=1;
			//get first segment
			let seg1 = self.get_seg_ref(i);
			let end0 = seg0.end();
			log!("end of Segment{} is {}", i-1, end0);
			let off1 = seg1.offset;
			log!("offset of Segment{} is {}", i, off1);

			//resolve gaps and overlaps
			if ((off1 > end0) && ((off1 - end0) > 1)) || off1 <= end0 {
				//logging stuff
				if off1 > end0 {
					log!("gap of size {} detected", fmtint(off1-end0));
				}
				if off1 <= end0 {
					log!("overlap of size {} detected", fmtint(end0-off1));
				}

				//actual code to resolve issues
				self.segs[i].set_offset(end0+1);
				self.check_size();
				log!("Segments {} and {} resolved", i-1, i);
			}
		}
		log!("done resolving SegmentTable");
		noplog!();
	}
	//convert a segment-relative address to an absolute address
	fn resolve_address(&self, addr: usize) -> usize {
		log!("resolving address {}", fmtint(addr));
		self.check_size();
		let curseg = self.get_cur_seg_ref();
		let offset = curseg.offset;
		let end = curseg.end();
		let computed = addr+offset;
		if computed > end {
			panic!("Attempted to access address out of current Segment boundary: {}", 
				fmtint(computed),
			);
		}
		log!("address {} resolved to {}", fmtint(addr), fmtint(computed));
		return computed;
	}
	//resize seg by index and call function to recalculate layout
	fn resize_seg(&mut self, seg: usize, size: usize) {
		log!("resizing Segment{} to size {}", seg, fmtint(size));
		self.segs[seg].size = size;
		self.check_size();
		self.resolve_layout();
	}
}


pub struct Memory {
	mem: [BASETYPE; MEMSIZE],//the memory array. Not to be directly accessed.
	segs: SegmentTable,//Segment manager, memory is to be accessed via segment abstraction
}

impl Memory {
	pub fn new() -> Memory {
		log!("creating Memory struct with MEMSIZE {}", fmtint(MEMSIZE));
		Memory {
			mem: [0x00; MEMSIZE],
			segs: SegmentTable::new(MEMSIZE),
		}
	}
	fn debug_segtable(&self) {
		log!("{:?}", self.segs);
	}
	//set the current segment to access values from
	pub fn set_cur_segment(&mut self, seg_num: usize) {
		self.segs.cur_seg = seg_num;
		log!("set current Segment to {}", seg_num);
	}
	//internal func for getting a single byte
	fn get(&self, addr: usize) -> u8 {
		let absolute = self.segs.resolve_address(addr);
		let got = self.mem[absolute];
		log!("got value {} from rel {}, abs {}", fmtint(got), fmtint(addr), fmtint(absolute));
		return got;
	}
	//internal func for setting a single byte
	fn set(&mut self, addr: usize, byte: u8) {
		let absolute = self.segs.resolve_address(addr);
		self.mem[absolute] = byte;
	}
	//create a u16 from two u8's
	fn compose_u16(&self, bytes: &[u8; 2]) -> u16 {
		let int = u16::from_be_bytes(*bytes);
		return int;
	}
	//create a u32 from four u8's
	fn compose_u32(&self, bytes: &[u8; 4]) -> u32 {
		let int = u32::from_be_bytes(*bytes);
		return int;
	}
	//create a u64 from 8 u8's
	fn compose_u64(&self, bytes: &[u8; 8]) -> u64 {
		let int = u64::from_be_bytes(*bytes);
		return int;
	}
	pub fn get_byte(&self, addr: usize) -> u8 {
		return self.get(addr);
	}
	pub fn set_byte(&mut self, addr: usize, value: u8) {
		self.set(addr, value);
	}
	//load a hex string of arbitrary length into memory starting at addr
	pub fn set_arbitrary(&mut self, addr: usize, value: &str) {
		let decoded = hex::decode(value).unwrap();
		for i in 0..decoded.len() {
			self.set(addr+i, decoded[i]);
		}
	}
	pub fn get_word(&self, addr: usize) -> u16 {
		let mut bytearray = [0u8; 2];
		for i in 0..2 {
			bytearray[i] = self.get(addr+i);
		}
		let word = self.compose_u16(&bytearray);
		log!("composed word: {}", fmtint(word));
		noplog!();
		return word;
	}
	pub fn get_dword(&self, addr: usize) -> u32 {
		let mut bytearray = [0u8; 4];
		for i in 0..4 {
			bytearray[i] = self.get(addr+i);
		}
		let dword = self.compose_u32(&bytearray);
		log!("composed dword: {}", fmtint(dword));
		noplog!();
		return dword;
	}
	pub fn get_qword(&self, addr: usize) -> u64 {
		let mut bytearray = [0u8; 8];
		for i in 0..8 {
			bytearray[i] = self.get(addr+i);
		}
		let qword = self.compose_u64(&bytearray);
		log!("composed qword: {}", fmtint(qword));
		noplog!();
		return qword;
	}
}
impl fmt::Debug for Memory {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "\n");
		for i in 0..self.mem.len() {
			write!(f, "{:#04X}: {:X};", i, self.mem[i]);
		}
		write!(f, "")
	}
}

